import pytest

from app.internal.services.card_services import card_transfer


class TestCardServices:
    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "source_card_id, recepient_card_id, sum, expected_status",
        [
            pytest.param("c2", "c1", 100, "forbidden", id="no_ownership_card_usage_forbidden"),
            pytest.param("c1", "none", 100, "no recipient", id="error_on_not_existing_card"),
            pytest.param("c1", "c2", 999999, "low balance", id="error_if_too_low_balance_to_transfer"),
            pytest.param("c1", "c2", 100, "success", id="correct_transfer"),
            pytest.param("c1", "c1", 100, "self", id="self_transfer"),
        ],
    )
    def test_card_transfer(
        self, two_persons_with_accs_and_cards, source_card_id, recepient_card_id, sum, expected_status
    ):
        person1, _ = two_persons_with_accs_and_cards
        status = card_transfer(person1.id, source_card_id, recepient_card_id, sum)
        assert status == expected_status
