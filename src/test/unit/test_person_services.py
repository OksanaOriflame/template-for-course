import pytest

from app.internal.services.person_services import (
    person_add_favorite_by_id,
    person_add_favorite_by_username,
    person_rem_favorite_by_id,
    person_rem_favorite_by_username,
)


class TestPersonServices:
    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "id_to_add, expected",
        [
            pytest.param("incorrect id", "none", id="error_on_not_existing_person"),
            pytest.param("id2", "exists", id="error_on_existing_favorite"),
            pytest.param("id1", "self", id="error_on_add_self"),
        ],
    )
    def test_person_add_favorite_by_id_errors(self, default_first_person_with_favorite, id_to_add, expected):
        first_person, _ = default_first_person_with_favorite
        status = person_add_favorite_by_id(first_person.id, id_to_add)
        assert status == expected

    @pytest.mark.django_db
    def test_person_add_favorite_by_id_success(self, default_first_person, default_second_person):
        status = person_add_favorite_by_id(default_first_person.id, default_second_person.id)
        assert status == "added"
        assert default_second_person in default_first_person.favorites.all()

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "id_to_rem, expected",
        [
            pytest.param("incorrect id", "none", id="error_on_not_existing_person"),
            pytest.param("id2", "not exists", id="error_on_not_existing_favorite"),
        ],
    )
    def test_person_rem_favorite_by_id_errors(self, default_first_person, default_second_person, id_to_rem, expected):
        status = person_rem_favorite_by_id(default_first_person.id, id_to_rem)
        assert status == expected

    @pytest.mark.django_db
    def test_person_rem_favorite_by_id_success(self, default_first_person_with_favorite):
        first_person, second_person = default_first_person_with_favorite
        status = person_rem_favorite_by_id(first_person.id, second_person.id)
        assert status == "removed"
        assert second_person not in first_person.favorites.all()

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "username_to_add, expected",
        [
            pytest.param("incorrect username", "none", id="error_on_not_existing_person"),
            pytest.param("un2", "exists", id="error_on_existing_favorite"),
            pytest.param("un1", "self", id="error_on_add_self"),
        ],
    )
    def test_person_add_favorite_by_username_errors(
        self, default_first_person_with_favorite, username_to_add, expected
    ):
        first_person, _ = default_first_person_with_favorite
        status = person_add_favorite_by_username(first_person.id, username_to_add)
        assert status == expected

    @pytest.mark.django_db
    def test_person_add_favorite_by_username_success(self, default_first_person, default_second_person):
        status = person_add_favorite_by_username(default_first_person.id, default_second_person.username)
        assert status == "added"
        assert default_second_person in default_first_person.favorites.all()

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "username_to_rem, expected",
        [
            pytest.param("incorrect username", "none", id="error_on_not_existing_person"),
            pytest.param("un2", "not exists", id="error_on_not_existing_favorite"),
        ],
    )
    def test_person_rem_favorite_by_username_errors(
        self, default_first_person, default_second_person, username_to_rem, expected
    ):
        status = person_rem_favorite_by_username(default_first_person.id, username_to_rem)
        assert status == expected

    @pytest.mark.django_db
    def test_person_rem_favorite_by_username_success(self, default_first_person_with_favorite):
        first_person, second_person = default_first_person_with_favorite
        status = person_rem_favorite_by_username(first_person.id, second_person.username)
        assert status == "removed"
        assert second_person not in first_person.favorites.all()
