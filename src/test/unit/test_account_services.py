import pytest

from app.internal.services.account_services import account_transfer, account_transfer_by_username


class TestAccountServices:
    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "source_acc_id, recepient_acc_id, sum, expected_status",
        [
            pytest.param("acc2", "acc1", 100, "forbidden", id="no_ownership_account_usage_forbidden"),
            pytest.param("acc1", "none", 100, "no recipient", id="error_on_not_existing_account"),
            pytest.param("acc1", "acc2", 999999, "low balance", id="error_if_too_low_balance_to_transfer"),
            pytest.param("acc1", "acc2", 100, "success", id="correct_transfer"),
            pytest.param("acc1", "acc1", 100, "self", id="self_transfer"),
        ],
    )
    def test_account_transfer_by_id(
        self, two_persons_with_accounts, source_acc_id, recepient_acc_id, sum, expected_status
    ):
        person1, _ = two_persons_with_accounts
        status = account_transfer(person1.id, source_acc_id, recepient_acc_id, sum)
        assert status == expected_status

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "recepient_username, sum, expected_status",
        [
            pytest.param("none", 100, "no recipient", id="error_on_not_existing_account"),
            pytest.param("un2", 999999, "low balance", id="error_if_too_low_balance_to_transfer"),
            pytest.param("un2", 100, "success", id="correct_transfer"),
            pytest.param("un1", 100, "self", id="self_transfer"),
        ],
    )
    def test_account_transfer_by_username(self, two_persons_with_accounts, recepient_username, sum, expected_status):
        person1, _ = two_persons_with_accounts
        status = account_transfer_by_username(person1.id, recepient_username, sum)
        assert status == expected_status

    @pytest.mark.django_db
    def test_account_transfer_by_username_error_on_no_recepient_accounts(
        self, default_first_person_with_account, default_second_person
    ):
        status = account_transfer_by_username(
            default_first_person_with_account[0].id, default_second_person.username, 100
        )
        assert status == "no recipient"

    @pytest.mark.django_db
    def test_account_transfer_by_username_error_on_no_source_accounts(
        self, default_first_person_with_account, default_second_person
    ):
        status = account_transfer_by_username(
            default_second_person.id, default_first_person_with_account[0].username, 100
        )
        assert status == "forbidden"
