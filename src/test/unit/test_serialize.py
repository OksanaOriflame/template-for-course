import pytest

from app.internal.infrastructure.serialize import (
    account_info,
    card_info,
    favorites_info,
    person_accounts_ids,
    person_cards_ids,
    person_info,
)


class TestSerialization:
    @pytest.mark.django_db
    def test_person_has_accounts_ids(self, default_first_person_with_account):
        person, acc = default_first_person_with_account
        data = person_accounts_ids(person.id)
        assert data == f"Your active accounts ids:\n\n{acc.id}"

    @pytest.mark.django_db
    def test_person_has_no_accounts_ids(self, default_first_person):
        data = person_accounts_ids(default_first_person.id)
        assert data == "You have no active accounts"

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "acc_id_to_check, expected",
        [
            pytest.param(
                "acc1", "account id: acc1\nowner id: id1\naccount balance: 10000.00", id="correct_account_info"
            ),
            pytest.param("incorrect id", "You don't have access to this account", id="no_info_if_no_account_access"),
        ],
    )
    def test_account_info(self, default_first_person_with_account, acc_id_to_check, expected):
        person, _ = default_first_person_with_account
        data = account_info(person.id, acc_id_to_check)
        assert data == expected

    @pytest.mark.django_db
    def test_person_has_card_ids(self, default_first_person_with_account_and_card):
        person, _, card = default_first_person_with_account_and_card
        data = person_cards_ids(person.id)
        assert data == f"Your active cards ids:\n\n{card.id}"

    @pytest.mark.django_db
    def test_person_has_no_cards_ids(self, default_first_person):
        data = person_cards_ids(default_first_person.id)
        assert data == "You have no active cards"

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "card_id_to_check, expected",
        [
            pytest.param(
                "c1",
                "card id: c1\nassociated account id: acc1\nowner id: id1\ncard balance: 10000.00",
                id="correct_card_info",
            ),
            pytest.param("incorrect id", "You don't have access to this card", id="no_info_if_no_card_access"),
        ],
    )
    def test_card_info(self, default_first_person_with_account_and_card, card_id_to_check, expected):
        person, _, _ = default_first_person_with_account_and_card
        data = card_info(person.id, card_id_to_check)
        assert data == expected

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "from_tg, expected",
        [
            pytest.param(True, "id: id1\nusername: un1\nphone number: pn1", id="correct_parse_for_tg"),
            pytest.param(False, "id: id1<br>username: un1<br>phone number: pn1", id="correct_parse_for_rest"),
        ],
    )
    def test_person_info(self, default_first_person, from_tg, expected):
        data = person_info(default_first_person.id, tg=from_tg)
        assert data == expected

    @pytest.mark.django_db
    def test_have_correct_favorite(self, default_first_person_with_favorite):
        person, persons_favorite = default_first_person_with_favorite
        data = favorites_info(person.id)
        assert data == [f"id : {persons_favorite.id}, username: {persons_favorite.username}"]

    @pytest.mark.django_db
    def test_have_no_favorites(self, default_first_person):
        data = favorites_info(default_first_person.id)
        assert data == []
