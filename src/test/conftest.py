import pytest

from app.models import Account, Card, Person
from app.internal.services.person_services import create_password


def make_person(id, username, phone_number):
    return Person.objects.create(id=id, username=username, phone_number=phone_number)


@pytest.fixture(scope="function")
def default_first_person():
    return make_person("id1", "un1", "pn1")


@pytest.fixture(scope="function")
def default_second_person():
    return make_person("id2", "un2", "pn2")


@pytest.fixture(scope="function")
def default_third_person():
    return make_person("id3", "un3", "pn3")


@pytest.fixture(scope="function")
def default_first_person_with_password(default_first_person):
    create_password(default_first_person.id, "password")
    return default_first_person


@pytest.fixture(scope="function")
def default_first_person_with_favorite(default_first_person, default_second_person):
    default_first_person.favorites.add(default_second_person)
    default_first_person.save()
    return default_first_person, default_second_person


@pytest.fixture(scope="function")
def default_first_person_with_account(default_first_person):
    acc = Account.objects.create(id="acc1", owner=default_first_person, balance=10000)
    return default_first_person, acc


@pytest.fixture(scope="function")
def default_second_person_with_account(default_second_person):
    acc = Account.objects.create(id="acc2", owner=default_second_person, balance=10000)
    return default_second_person, acc


@pytest.fixture(scope="function")
def two_persons_with_accounts(default_first_person_with_account, default_second_person_with_account):
    return default_first_person_with_account[0], default_second_person_with_account[0]


@pytest.fixture(scope="function")
def default_first_person_with_account_and_card(default_first_person_with_account):
    person, acc = default_first_person_with_account
    card = Card.objects.create(id="c1", account=acc)
    return person, acc, card


@pytest.fixture(scope="function")
def default_second_person_with_account_and_card(default_second_person_with_account):
    person, acc = default_second_person_with_account
    card = Card.objects.create(id="c2", account=acc)
    return person, acc, card


@pytest.fixture(scope="function")
def two_persons_with_accs_and_cards(
    default_first_person_with_account_and_card, default_second_person_with_account_and_card
):
    return default_first_person_with_account_and_card[0], default_second_person_with_account_and_card[0]
