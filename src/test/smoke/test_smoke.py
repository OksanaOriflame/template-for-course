import os

import environ
import pytest
import telegram
import json

from app.internal.bot.bot_webhook import get_bot


class Test_smoke:
    def test_bot(self):
        env = environ.Env()
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        environ.Env.read_env(os.path.join(BASE_DIR, ".env"))

        updater = get_bot(env("TG_BOT_TOKEN"))
        updater.dispatcher.process_update(telegram.Update.de_json({}, updater.bot))

    @pytest.mark.django_db
    def test_me_person_with_password(self, client, default_first_person_with_password):
        url = "/api/me"
        auth_url = "/api/auth"
        authentication_response = client.post(auth_url, HTTP_Login=default_first_person_with_password.username, HTTP_Password="password")
        access_token = json.loads(authentication_response.content)["access_token"]
        print(access_token)
        response = client.get(url, HTTP_Authorization=f"bearer {access_token}")
        assert response.status_code == 200

    @pytest.mark.django_db
    def test_me_person_without_password(self, client, default_first_person):
        url = f"/api/me?id={default_first_person.id}"
        response = client.get(url)
        assert response.status_code == 401
