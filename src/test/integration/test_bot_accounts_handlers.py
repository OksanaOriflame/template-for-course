from unittest.mock import PropertyMock

import pytest

from app.internal.transport.bot.accounts_handler import account_info_handler, accounts
from app.internal.transport.bot.message_constants import ACCOUNT_INFO_TIP


class TestBotAccounts:
    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "custom_telegram_update, expected",
        [
            pytest.param(
                {"id": "id1", "un": "un1", "text": ""},
                f"Your active accounts ids:\n\nacc1\n\n{ACCOUNT_INFO_TIP}",
                id="person_with_acc",
            ),
            pytest.param(
                {"id": "id2", "un": "un2", "text": ""},
                f"You have no active accounts\n\n{ACCOUNT_INFO_TIP}",
                id="person_without_acc",
            ),
        ],
        indirect=["custom_telegram_update"],
    )
    def test_accounts(
        self,
        custom_telegram_update,
        telegram_callback,
        default_first_person_with_account,
        default_second_person,
        expected,
    ):
        accounts(custom_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=custom_telegram_update.effective_chat.id, text=expected
        )

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "custom_telegram_update, expected",
        [
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/account acc1"},
                "account id: acc1\nowner id: id1\naccount balance: 10000.00",
                id="own_acc",
            ),
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/account acc2"},
                "You don't have access to this account",
                id="no_ownership_acc",
            ),
        ],
        indirect=["custom_telegram_update"],
    )
    def test_account_info_handler(
        self, custom_telegram_update, telegram_callback, two_persons_with_accs_and_cards, expected
    ):
        account_info_handler(custom_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=custom_telegram_update.effective_chat.id, text=expected
        )
