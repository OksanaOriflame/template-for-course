import pytest

from app.internal.transport.bot.info_handlers import me, unrecognized
from app.internal.transport.bot.message_constants import POSSIBLE_OPTIONS


class TestBotInfo:
    @pytest.mark.django_db
    def test_unrecognized(self, default_telegram_update, telegram_callback):
        unrecognized(default_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id="id1", text=f"Sorry, I don't understand you. You can use one of my options\n\n{POSSIBLE_OPTIONS}"
        )

    @pytest.mark.django_db
    def test_me(self, default_telegram_update, telegram_callback):
        me(default_telegram_update, telegram_callback)
        expected_info = "id: id1\nusername: un1\nphone number: pn1"
        telegram_callback.bot.send_message.assert_called_with(
            chat_id="id1", text=f"That's all I know about you for now:\n\n{expected_info}"
        )
