from unittest.mock import MagicMock, PropertyMock

import pytest


def get_telegram_update(id, username, text=""):
    update = MagicMock()
    update.effective_chat.id = id
    update.effective_chat.username = username
    update.effective_message.text = text
    return update


@pytest.fixture(scope="function")
def default_telegram_update(default_first_person):
    return get_telegram_update(default_first_person.id, default_first_person.username)


@pytest.fixture(scope="function")
def custom_telegram_update(request):
    return get_telegram_update(request.param["id"], request.param["un"], request.param["text"])


@pytest.fixture(scope="function")
def default_telegram_update_with_custom_text(default_first_person, request):
    return get_telegram_update(default_first_person.id, default_first_person.username, request.param)


@pytest.fixture(scope="function")
def telegram_update_with_two_person_and_custom_text(two_persons_with_accs_and_cards, request):
    person1, _ = two_persons_with_accs_and_cards
    return get_telegram_update(person1.id, person1.username, request.param)


@pytest.fixture(scope="function")
def telegram_callback():
    callback = MagicMock()
    return callback


@pytest.fixture(scope="function")
def rest_request_with_custom_user_query(request):
    request_ = MagicMock()
    request_.bearer_id = request.param
    return request_
