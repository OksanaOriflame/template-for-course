import pytest

from app.internal.transport.bot.message_constants import TRANSFER_INFO, TRANSFER_INVALID_COMMAND, TRANSFER_REPLIES
from app.internal.transport.bot.transfer_handlers import transfer_account, transfer_card, transfer_info, transfer_user


class TestBotAccounts:
    @pytest.mark.django_db
    def test_transfer_info(self, default_telegram_update, telegram_callback):
        transfer_info(default_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(chat_id="id1", text=TRANSFER_INFO)

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "telegram_update_with_two_person_and_custom_text, expected",
        [
            pytest.param("/cardtransfer incorrect input", TRANSFER_INVALID_COMMAND, id="error_on_incorrect_command"),
            pytest.param(
                "/cardtransfer c2 c1 100", TRANSFER_REPLIES["forbidden"], id="no_ownership_card_usage_forbidden"
            ),
            pytest.param(
                "/cardtransfer c1 none 100", TRANSFER_REPLIES["no recipient"], id="error_on_not_existing_recepient"
            ),
            pytest.param("/cardtransfer c1 c2 999999", TRANSFER_REPLIES["low balance"], id="error_on_low_balance"),
            pytest.param("/cardtransfer c1 c2 100", TRANSFER_REPLIES["success"], id="success_if_correct"),
            pytest.param("/cardtransfer c1 c1 100", TRANSFER_REPLIES["self"], id="same_source_and_destination"),
        ],
        indirect=["telegram_update_with_two_person_and_custom_text"],
    )
    def test_transfer_card(self, telegram_update_with_two_person_and_custom_text, telegram_callback, expected):
        transfer_card(telegram_update_with_two_person_and_custom_text, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(chat_id="id1", text=expected)

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "telegram_update_with_two_person_and_custom_text, expected",
        [
            pytest.param("/acctransfer incorrect input", TRANSFER_INVALID_COMMAND, id="error_on_incorrect_command"),
            pytest.param(
                "/acctransfer acc2 acc1 100", TRANSFER_REPLIES["forbidden"], id="no_ownership_account_usage_forbidden"
            ),
            pytest.param(
                "/acctransfer acc1 none 100", TRANSFER_REPLIES["no recipient"], id="error_on_not_existing_recepient"
            ),
            pytest.param("/acctransfer acc1 acc2 999999", TRANSFER_REPLIES["low balance"], id="error_on_low_balance"),
            pytest.param("/acctransfer acc1 acc2 100", TRANSFER_REPLIES["success"], id="success_if_correct"),
            pytest.param("/acctransfer acc1 acc1 100", TRANSFER_REPLIES["self"], id="same_source_and_destination"),
        ],
        indirect=["telegram_update_with_two_person_and_custom_text"],
    )
    def test_transfer_account(self, telegram_update_with_two_person_and_custom_text, telegram_callback, expected):
        transfer_account(telegram_update_with_two_person_and_custom_text, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(chat_id="id1", text=expected)

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "telegram_update_with_two_person_and_custom_text, expected",
        [
            pytest.param(
                "/usertransfer super incorrect input", TRANSFER_INVALID_COMMAND, id="error_on_incorrect_command"
            ),
            pytest.param(
                "/usertransfer none 100", TRANSFER_REPLIES["no recipient"], id="no_ownership_account_usage_forbidden"
            ),
            pytest.param(
                "/usertransfer un3 100", TRANSFER_REPLIES["no recipient"], id="error_on_not_existing_recepient"
            ),
            pytest.param("/usertransfer un2 999999", TRANSFER_REPLIES["low balance"], id="error_on_low_balance"),
            pytest.param("/usertransfer un2 100", TRANSFER_REPLIES["success"], id="success_if_correct"),
            pytest.param("/usertransfer un1 100", TRANSFER_REPLIES["self"], id="same_source_and_destination"),
        ],
        indirect=["telegram_update_with_two_person_and_custom_text"],
    )
    def test_transfer_user(
        self, telegram_update_with_two_person_and_custom_text, telegram_callback, default_third_person, expected
    ):
        transfer_user(telegram_update_with_two_person_and_custom_text, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(chat_id="id1", text=expected)

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "default_telegram_update_with_custom_text",
        ["/usertransfer un2 100"],
        indirect=["default_telegram_update_with_custom_text"],
    )
    def test_transfer_user_no_account(
        self, default_telegram_update_with_custom_text, telegram_callback, default_second_person_with_account_and_card
    ):
        transfer_user(default_telegram_update_with_custom_text, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(chat_id="id1", text=TRANSFER_REPLIES["forbidden"])
