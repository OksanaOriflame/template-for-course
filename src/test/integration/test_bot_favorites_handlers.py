import pytest

from app.internal.transport.bot.favorites_handlers import (
    add_favorite_by_id,
    add_favorite_by_username,
    favorites,
    rem_favorite_by_id,
    rem_favorite_by_username,
)
from app.internal.transport.bot.message_constants import FAV_REPLIES, FAVS_TIP, NO_FAVS


class TestBotFavorites:
    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "custom_telegram_update, expected",
        [
            pytest.param(
                {"id": "id1", "un": "un1", "text": ""}, "id : id2, username: un2" + FAVS_TIP, id="person_with_fav"
            ),
            pytest.param({"id": "id2", "un": "un2", "text": ""}, NO_FAVS + FAVS_TIP, id="person_without_favs"),
        ],
        indirect=["custom_telegram_update"],
    )
    def test_favorites(self, custom_telegram_update, telegram_callback, default_first_person_with_favorite, expected):
        favorites(custom_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=custom_telegram_update.effective_chat.id, text=expected
        )

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "custom_telegram_update, expected",
        [
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/addfav none"}, FAV_REPLIES["none"], id="add_not_existing_person"
            ),
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/addfav id2"}, FAV_REPLIES["exists"], id="add_existing_favorite"
            ),
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/addfav id3"}, FAV_REPLIES["added"], id="add_not_existing_favorite"
            ),
            pytest.param({"id": "id1", "un": "un1", "text": "/addfav id1"}, FAV_REPLIES["self"], id="add_yourself"),
        ],
        indirect=["custom_telegram_update"],
    )
    def test_add_favorite_by_id(
        self,
        custom_telegram_update,
        telegram_callback,
        default_first_person_with_favorite,
        default_third_person,
        expected,
    ):
        add_favorite_by_id(custom_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=custom_telegram_update.effective_chat.id, text=expected
        )

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "custom_telegram_update, expected",
        [
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/remfav none"}, FAV_REPLIES["none"], id="rem_not_existing_person"
            ),
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/remfav id3"},
                FAV_REPLIES["not exists"],
                id="rem_not_existing_favorite",
            ),
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/remfav id2"}, FAV_REPLIES["removed"], id="rem_existing_favorite"
            ),
        ],
        indirect=["custom_telegram_update"],
    )
    def test_rem_favorite_by_id(
        self,
        custom_telegram_update,
        telegram_callback,
        default_first_person_with_favorite,
        default_third_person,
        expected,
    ):
        rem_favorite_by_id(custom_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=custom_telegram_update.effective_chat.id, text=expected
        )

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "custom_telegram_update, expected",
        [
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/addfavname none"},
                FAV_REPLIES["none"],
                id="add_not_existing_person",
            ),
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/addfavname un2"}, FAV_REPLIES["exists"], id="add_existing_favorite"
            ),
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/addfavname un3"},
                FAV_REPLIES["added"],
                id="add_not_existing_favorite",
            ),
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/addfavname un1"},
                FAV_REPLIES["self"],
                id="add_yourself",
            ),
        ],
        indirect=["custom_telegram_update"],
    )
    def test_add_favorite_by_username(
        self,
        custom_telegram_update,
        telegram_callback,
        default_first_person_with_favorite,
        default_third_person,
        expected,
    ):
        add_favorite_by_username(custom_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=custom_telegram_update.effective_chat.id, text=expected
        )

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "custom_telegram_update, expected",
        [
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/remfavname none"},
                FAV_REPLIES["none"],
                id="rem_not_existing_person",
            ),
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/remfavname un3"},
                FAV_REPLIES["not exists"],
                id="rem_not_existing_favorite",
            ),
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/remfavname un2"},
                FAV_REPLIES["removed"],
                id="rem_existing_favorite",
            ),
        ],
        indirect=["custom_telegram_update"],
    )
    def test_rem_favorite_by_username(
        self,
        custom_telegram_update,
        telegram_callback,
        default_first_person_with_favorite,
        default_third_person,
        expected,
    ):
        rem_favorite_by_username(custom_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=custom_telegram_update.effective_chat.id, text=expected
        )
