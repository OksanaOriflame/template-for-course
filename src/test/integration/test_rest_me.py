import pytest

from app.internal.transport.rest.handlers import MeView


class TestRestHandlers:
    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "rest_request_with_custom_user_query, expected",
        [
            pytest.param(
                "id1", b"id: id1<br>username: un1<br>phone number: pn1", id="existing_person_id"
            ),
            pytest.param("none", b"No user with id none", id="non-existent_person_id"),
        ],
        indirect=["rest_request_with_custom_user_query"],
    )
    def test_me(self, rest_request_with_custom_user_query, default_first_person, expected):
        httpResponse = MeView().get(rest_request_with_custom_user_query)
        assert httpResponse.content == expected
