import pytest

from app.internal.transport.bot.message_constants import PHONE_ACK, PHONE_NACK, PHONE_REQUEST
from app.internal.transport.bot.set_phone_handlers import set_phone, set_phone_start


class TestBotSetPhone:
    @pytest.mark.django_db
    def test_set_phone_start(self, default_telegram_update, telegram_callback):
        set_phone_start(default_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=default_telegram_update.effective_chat.id, text=PHONE_REQUEST
        )

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "default_telegram_update_with_custom_text, expected",
        [
            pytest.param("bad number", PHONE_NACK, id="incorrect_phone_number"),
            pytest.param("+79129999999", PHONE_ACK, id="correct_phone_number"),
        ],
        indirect=["default_telegram_update_with_custom_text"],
    )
    def test_set_phone(self, default_telegram_update_with_custom_text, telegram_callback, expected):
        set_phone(default_telegram_update_with_custom_text, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=default_telegram_update_with_custom_text.effective_chat.id, text=expected
        )
