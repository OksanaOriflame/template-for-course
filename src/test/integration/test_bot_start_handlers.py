import pytest

from app.internal.transport.bot.message_constants import NO_PHONE_ANS, POSSIBLE_OPTIONS, UNKNOWN_PERSON_ANS
from app.internal.transport.bot.start_handlers import not_initialized, start


class TestBotStart:
    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "custom_telegram_update, expected",
        [
            pytest.param(
                {"id": "id1", "un": "un1", "text": ""},
                f"I already remember you! Your id is id1\n\n{POSSIBLE_OPTIONS}",
                id="correct_ans_for_existed_person",
            ),
            pytest.param(
                {"id": "id2", "un": "un2", "text": ""},
                f"Hello! I remembered you. Your id is id2{NO_PHONE_ANS}\n\n{POSSIBLE_OPTIONS}",
                id="correct_ans_for_not_existed_person",
            ),
        ],
        indirect=["custom_telegram_update"],
    )
    def test_start(self, custom_telegram_update, telegram_callback, default_first_person, expected):
        start(custom_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=custom_telegram_update.effective_chat.id, text=expected
        )

    @pytest.mark.django_db
    def test_not_initialized(self, default_telegram_update, telegram_callback):
        not_initialized(default_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=default_telegram_update.effective_chat.id, text=UNKNOWN_PERSON_ANS
        )
