import pytest

from app.internal.transport.bot.cards_handlers import card_info_handler, cards
from app.internal.transport.bot.message_constants import CARD_INFO_TIP


class TestBotAccounts:
    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "custom_telegram_update, expected",
        [
            pytest.param(
                {"id": "id1", "un": "un1", "text": ""},
                f"Your active cards ids:\n\nc1\n\n{CARD_INFO_TIP}",
                id="person_with_card",
            ),
            pytest.param(
                {"id": "id2", "un": "un2", "text": ""},
                f"You have no active cards\n\n{CARD_INFO_TIP}",
                id="person_without_card",
            ),
        ],
        indirect=["custom_telegram_update"],
    )
    def test_cards(
        self,
        custom_telegram_update,
        telegram_callback,
        default_first_person_with_account_and_card,
        default_second_person,
        expected,
    ):
        cards(custom_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=custom_telegram_update.effective_chat.id, text=expected
        )

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "custom_telegram_update, expected",
        [
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/card c1"},
                "card id: c1\nassociated account id: acc1\nowner id: id1\ncard balance: 10000.00",
                id="own_card",
            ),
            pytest.param(
                {"id": "id1", "un": "un1", "text": "/card c2"},
                "You don't have access to this card",
                id="no_ownership_card",
            ),
        ],
        indirect=["custom_telegram_update"],
    )
    def test_card_info_handler(
        self, custom_telegram_update, telegram_callback, two_persons_with_accs_and_cards, expected
    ):
        card_info_handler(custom_telegram_update, telegram_callback)
        telegram_callback.bot.send_message.assert_called_with(
            chat_id=custom_telegram_update.effective_chat.id, text=expected
        )
