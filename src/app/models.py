from django.db import models

from app.internal.models.admin_user import AdminUser


class Person(models.Model):
    id = models.CharField(primary_key=True, blank=False, max_length=12)
    username = models.CharField(max_length=20, unique=True, blank=False)
    phone_number = models.TextField(default="none")
    favorites = models.ManyToManyField("self", symmetrical=False, blank=True)
    password_hash = models.CharField(max_length=256, blank=True, null=True, default=None)


class Account(models.Model):
    id = models.CharField(primary_key=True, blank=False, max_length=12)
    owner = models.ForeignKey(Person, on_delete=models.CASCADE, blank=False)
    balance = models.DecimalField(default=0, decimal_places=2, max_digits=20)


class Card(models.Model):
    id = models.CharField(primary_key=True, blank=False, max_length=12)
    account = models.ForeignKey(Account, on_delete=models.CASCADE, blank=False)


class AccountOperation(models.Model):
    source_account = models.ForeignKey(Account, related_name="src_acc", on_delete=models.CASCADE, blank=False)
    recepient_account = models.ForeignKey(Account, related_name="rcp_acc", on_delete=models.CASCADE, blank=False)
    time = models.DateTimeField(blank=False, db_index=True)
    sum = models.DecimalField(blank=False, decimal_places=2, max_digits=20)


class CardOperation(models.Model):
    source_card = models.ForeignKey(Card, related_name="src_card", on_delete=models.CASCADE, blank=False)
    recepient_card = models.ForeignKey(Card, related_name="rcp_card", on_delete=models.CASCADE, blank=False)
    time = models.DateTimeField(blank=False, db_index=True)
    sum = models.DecimalField(blank=False, decimal_places=2, max_digits=20)


class IssuedToken(models.Model):
    jti = models.CharField(max_length=256, primary_key=True, blank=False)
    user = models.ForeignKey(Person, blank=False, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    expires_at = models.DateTimeField(blank=False)
    revoked = models.BooleanField(default=False)
