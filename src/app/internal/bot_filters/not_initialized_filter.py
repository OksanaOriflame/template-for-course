from telegram.ext import MessageFilter

from app.models import Person


class Not_initialized_filter(MessageFilter):
    def filter(self, message):
        chat_id = message.chat.id
        return not Person.objects.filter(id=chat_id).exists()
