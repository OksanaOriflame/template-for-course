from telegram.ext import MessageFilter

from app.models import Person


class Setting_phone_filter(MessageFilter):
    def filter(self, message):
        chat_id = message.chat.id
        person = Person.objects.get(id=chat_id)
        return person.phone_number == "none"
