from telegram import Update
from telegram.ext import CallbackContext

from app.internal.infrastructure.serialize import account_info, person_accounts_ids
from app.internal.transport.bot.message_constants import ACCOUNT_INFO_TIP


def accounts(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    accounts_info = person_accounts_ids(chat_id)
    context.bot.send_message(chat_id=chat_id, text=f"{accounts_info}\n\n{ACCOUNT_INFO_TIP}")


def account_info_handler(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    account_id = update.effective_message.text[len("/account ") :]
    context.bot.send_message(chat_id=chat_id, text=account_info(chat_id, account_id))
