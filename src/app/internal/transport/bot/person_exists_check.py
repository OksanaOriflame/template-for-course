from app.models import Person

from .message_constants import NO_PHONE_ANS, UNKNOWN_PERSON_ANS


def check_person_exists_and_filled_phone_number(person_id):
    person = Person.objects.filter(id=person_id).first()
    if not person:
        return UNKNOWN_PERSON_ANS
    elif person.phone_number == "none":
        return NO_PHONE_ANS
    return None
