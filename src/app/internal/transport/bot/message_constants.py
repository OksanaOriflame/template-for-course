from app.internal.services.status_constants import OpServiceStatus

POSSIBLE_OPTIONS = "Possible bot options:\n/start - Begin to use bot and get possible options\n/me - Get information about you\n/set_phone - Set your phone number\n\
/cards - Get information about cards\n/accounts - Get information about accounts\n/favorites - Get list of favorite users\n/transferinfo - Get Info about transfer abilities\n\
/connections - Get all connected to you persons usernames\n/acchistory {account id} - Get account operations info\n/cardhistory {card id} - Get card operations info"

TRANSFER_INFO = "To transfer money from card to card use:\n/cardtransfer {source card id} {destination card id} {sum}\n\n\
    To transfer money from account to account use:\n/acctransfer {source account id} {destination account id} {sum}\n\n\
    To transfer money from user to user use:\n/usertransfer {destination user name} {sum}"

UNKNOWN_PERSON_ANS = "I don't know, who are you(\nUse /start command to begin conversation!"

NO_PHONE_ANS = (
    "\n\nYou can't use all services until you tell me your phone number. I am waiting for It in the next message"
)

PHONE_REQUEST = "Tell me your phone number"

PHONE_ACK = "Now I know your phone number!"

PHONE_NACK = (
    "Your number is not valid. Perhaps you forgot "
    + " in the beginning? Try again.\n\nTo stop setting the number use /start option"
)

CARD_INFO_TIP = "To get card detailing use command with parameter:\n/card {your card id}"

ACCOUNT_INFO_TIP = "To get account detailing use command with parameter:\n/account {your account id}"

NO_FAVS = "You don't have favorite users"

FAVS = "Your favorite users ids:\n"

FAVS_TIP = "\n\nYou can add favorite user by It's username using /addfavname {user name}\nYou can remove user from favorites list using /remfavname {user name}\
    \nYou can add favorite user by It's id using /addfav {user id}\nYou can remove user from favorites list using /remfav {user id}"

FAV_EXISTS = "Person with this id already exists in your favorites list"

FAV_NOT_EXISTS = "Person with this id does not exist in your favorites list"

FAV_NONE = "No Person with this id"

FAV_ADDED = "Person has been added to your fovorites list successfully"

FAV_REMOVED = "Person has been removed from your fovorites list successfully"

FAV_SELF = "Can not add you to your favorites"

FAV_REPLIES = {
    "exists": FAV_EXISTS,
    "added": FAV_ADDED,
    "not exists": FAV_NOT_EXISTS,
    "removed": FAV_REMOVED,
    "none": FAV_NONE,
    "self": FAV_SELF,
}

TRANSFER_INVALID_COMMAND = "Command format is invalid"

TRANSFER_FORBIDDEN = "You do not have permission for this operation"

TRANSFER_NO_RECIPIENT = "Incorrect recipient"

TRANSFER_LOW_BALANCE = "Too low balance to proceed"

TRANSFER_SUCCESS = "Operation succeded!"

TRANSFER_SELF = "Destination and source are the same"

TRANSFER_REPLIES = {
    "forbidden": TRANSFER_FORBIDDEN,
    "no recipient": TRANSFER_NO_RECIPIENT,
    "low balance": TRANSFER_LOW_BALANCE,
    "success": TRANSFER_SUCCESS,
    "self": TRANSFER_SELF,
}

OPERATION_REPLIES = {
    OpServiceStatus.EMPTY: "There's no operations yet",
    OpServiceStatus.FORBIDDEN: "No permission for information",
    OpServiceStatus.OK: "All operations:\n\n",
}

NO_CONNECTED_PERSONS = "You have no connected persons"

CONNECTED_PERSONS_HEADER = "All your connected persons usernames:\n\n"

AUTH_NO_PASSWORD_REPLY = "Please specify your password after /login command"

AUTH_PASSWORD_FORMAT = "Password length must be between 3 and 32 symbols and have no space characters"

AUTH_INCORRECT_PASSWORD_REPLY = "Invalid password format"

AUTH_SET_PASSWORD_SUCCESS = "Your password updated successfully. Use your telegram username and new password to authenticate"
