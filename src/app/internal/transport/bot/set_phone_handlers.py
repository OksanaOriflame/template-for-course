import phonenumbers
from telegram import Update
from telegram.ext import CallbackContext

from app.internal.transport.bot.message_constants import PHONE_ACK, PHONE_NACK, PHONE_REQUEST
from app.models import Person


def set_phone_start(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    person = Person.objects.get(id=chat_id)
    person.phone_number = "none"
    person.save()
    context.bot.send_message(chat_id=chat_id, text=PHONE_REQUEST)


def set_phone(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    number = parse_phone_number(update.effective_message.text)
    if number and phonenumbers.is_valid_number(number):
        international_format_number = phonenumbers.format_number(number, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
        person = Person.objects.get(id=chat_id)
        person.phone_number = international_format_number
        person.save()
        context.bot.send_message(chat_id=chat_id, text=PHONE_ACK)
    else:
        context.bot.send_message(
            chat_id=chat_id,
            text=PHONE_NACK,
        )


def parse_phone_number(number):
    try:
        parsed_number = phonenumbers.parse(number)
        return parsed_number
    except phonenumbers.phonenumberutil.NumberParseException:
        return None
