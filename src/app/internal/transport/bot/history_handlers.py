from telegram import Update
from telegram.ext import CallbackContext

from app.internal.infrastructure.serialize import (
    serialize_acc_operations,
    serialize_card_operations,
    serialize_username_list,
)
from app.internal.services.operation_services import account_history, card_history, connected_persons
from app.internal.services.status_constants import OpServiceStatus

from .message_constants import CONNECTED_PERSONS_HEADER, NO_CONNECTED_PERSONS, OPERATION_REPLIES


def card_history_handler(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    card_id = update.effective_message.text[len("/cardhistory ") :]
    operations_list, status = card_history(chat_id, card_id)
    if not status == OpServiceStatus.OK:
        context.bot.send_message(chat_id=chat_id, text=OPERATION_REPLIES[status])
        return
    response_text = serialize_card_operations(card_id, operations_list)
    context.bot.send_message(chat_id=chat_id, text=OPERATION_REPLIES[status] + response_text)


def account_history_handler(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    acc_id = update.effective_message.text[len("/acchistory ") :]
    operations_list, status = account_history(chat_id, acc_id)
    if not status == OpServiceStatus.OK:
        context.bot.send_message(chat_id=chat_id, text=OPERATION_REPLIES[status])
        return
    response_text = serialize_acc_operations(acc_id, operations_list)
    context.bot.send_message(chat_id=chat_id, text=OPERATION_REPLIES[status] + response_text)


def connected_persons_handler(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    username = update.effective_chat.username
    usernames_list = connected_persons(chat_id, username)
    if len(usernames_list) == 0:
        context.bot.send_message(chat_id=chat_id, text=NO_CONNECTED_PERSONS)
        return
    response = serialize_username_list(usernames_list)
    context.bot.send_message(chat_id=chat_id, text=CONNECTED_PERSONS_HEADER + response)
