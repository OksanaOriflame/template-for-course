from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.account_services import account_transfer, account_transfer_by_username
from app.internal.services.card_services import card_transfer
from app.internal.transport.bot.message_constants import TRANSFER_INFO, TRANSFER_INVALID_COMMAND, TRANSFER_REPLIES


def transfer_card(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    info = update.effective_message.text[len("/cardtransfer ") :].split()
    if len(info) != 3:
        context.bot.send_message(chat_id=chat_id, text=TRANSFER_INVALID_COMMAND)
        return
    from_id, to_id, sum = info
    result = card_transfer(chat_id, from_id, to_id, int(sum))
    context.bot.send_message(chat_id=chat_id, text=TRANSFER_REPLIES[result])


def transfer_account(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    info = update.effective_message.text[len("/acctransfer ") :].split()
    if len(info) != 3:
        context.bot.send_message(chat_id=chat_id, text=TRANSFER_INVALID_COMMAND)
        return
    from_id, to_id, sum = info
    result = account_transfer(chat_id, from_id, to_id, int(sum))
    context.bot.send_message(chat_id=chat_id, text=TRANSFER_REPLIES[result])


def transfer_user(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    info = update.effective_message.text[len("/usertransfer ") :].split()
    if len(info) != 2:
        context.bot.send_message(chat_id=chat_id, text=TRANSFER_INVALID_COMMAND)
        return
    to_username, sum = info
    result = account_transfer_by_username(chat_id, to_username, int(sum))
    context.bot.send_message(chat_id=chat_id, text=TRANSFER_REPLIES[result])


def transfer_info(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    context.bot.send_message(chat_id=chat_id, text=TRANSFER_INFO)
