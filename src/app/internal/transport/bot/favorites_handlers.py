from telegram import Update
from telegram.ext import CallbackContext

from app.internal.infrastructure.serialize import favorites_info
from app.internal.services.person_services import (
    person_add_favorite_by_id,
    person_add_favorite_by_username,
    person_rem_favorite_by_id,
    person_rem_favorite_by_username,
)
from app.internal.transport.bot.message_constants import FAV_REPLIES, FAVS_TIP, NO_FAVS


def favorites(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    favorite_persons = favorites_info(chat_id)
    if len(favorite_persons) == 0:
        context.bot.send_message(chat_id=chat_id, text=NO_FAVS + FAVS_TIP)
    else:
        content = "\n".join(favorite_persons)
        context.bot.send_message(chat_id=chat_id, text=content + FAVS_TIP)


def add_favorite_by_id(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    user_id = update.effective_message.text[len("/addfav ") :]
    status = person_add_favorite_by_id(chat_id, user_id)
    context.bot.send_message(chat_id=chat_id, text=FAV_REPLIES[status])


def rem_favorite_by_id(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    user_id = update.effective_message.text[len("/remfav ") :]
    status = person_rem_favorite_by_id(chat_id, user_id)
    context.bot.send_message(chat_id=chat_id, text=FAV_REPLIES[status])


def add_favorite_by_username(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    username = update.effective_message.text[len("/addfavname ") :]
    status = person_add_favorite_by_username(chat_id, username)
    context.bot.send_message(chat_id=chat_id, text=FAV_REPLIES[status])


def rem_favorite_by_username(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    username = update.effective_message.text[len("/remfavname ") :]
    status = person_rem_favorite_by_username(chat_id, username)
    context.bot.send_message(chat_id=chat_id, text=FAV_REPLIES[status])
