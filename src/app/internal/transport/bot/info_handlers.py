from telegram import Update
from telegram.ext import CallbackContext

from app.internal.infrastructure.serialize import person_info
from app.internal.transport.bot.message_constants import POSSIBLE_OPTIONS


def unrecognized(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    context.bot.send_message(
        chat_id=chat_id, text=f"Sorry, I don't understand you. You can use one of my options\n\n{POSSIBLE_OPTIONS}"
    )


def me(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    context.bot.send_message(chat_id=chat_id, text=f"That's all I know about you for now:\n\n{person_info(chat_id)}")
