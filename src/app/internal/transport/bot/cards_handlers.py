from telegram import Update
from telegram.ext import CallbackContext

from app.internal.infrastructure.serialize import card_info, person_cards_ids
from app.internal.transport.bot.message_constants import CARD_INFO_TIP


def cards(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    cards_info = person_cards_ids(chat_id)
    context.bot.send_message(chat_id=chat_id, text=f"{cards_info}\n\n{CARD_INFO_TIP}")


def card_info_handler(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    card_id = update.effective_message.text[len("/card ") :]
    context.bot.send_message(chat_id=chat_id, text=card_info(chat_id, card_id))
