import jwt
from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.person_services import create_password

from .person_exists_check import check_person_exists_and_filled_phone_number
from .message_constants import AUTH_NO_PASSWORD_REPLY, AUTH_PASSWORD_FORMAT, AUTH_INCORRECT_PASSWORD_REPLY, AUTH_SET_PASSWORD_SUCCESS


def login(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    no_person_ans = check_person_exists_and_filled_phone_number(chat_id)
    if no_person_ans:
        context.bot.send_message(chat_id=chat_id, text=no_person_ans)
        return
    text = update.effective_message.text
    if len(text) < 7:
        context.bot.send_message(chat_id=chat_id, text=AUTH_NO_PASSWORD_REPLY + "\n\n" + AUTH_PASSWORD_FORMAT)
        return
    password = text[len("/login ") :].split()
    if len(password) != 1 or len(password[0]) < 3 or len(password[0]) > 32:
        context.bot.send_message(chat_id=chat_id, text=AUTH_INCORRECT_PASSWORD_REPLY + "\n\n" + AUTH_PASSWORD_FORMAT)
        return
    password_set_status = create_password(chat_id, password[0])
    if not password_set_status:
        context.bot.send_message(chat_id=chat_id, text=AUTH_INCORRECT_PASSWORD_REPLY + "\n\n" + AUTH_PASSWORD_FORMAT)
        return
    context.bot.send_message(chat_id=chat_id, text=AUTH_SET_PASSWORD_SUCCESS)
