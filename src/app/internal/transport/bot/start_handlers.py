from telegram import Update
from telegram.ext import CallbackContext

from app.internal.transport.bot.message_constants import NO_PHONE_ANS, POSSIBLE_OPTIONS, UNKNOWN_PERSON_ANS
from app.models import Person


def start(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    username = update.effective_chat.username
    person = Person.objects.filter(id=chat_id).first()
    phone_number_notification = NO_PHONE_ANS
    if not person:
        person = Person.objects.create(id=chat_id, username=username)
        context.bot.send_message(
            chat_id=chat_id,
            text=f"Hello! I remembered you. Your id is {chat_id}{phone_number_notification}\n\n{POSSIBLE_OPTIONS}",
        )
    else:
        phone_number = person.phone_number
        if phone_number != "none":
            phone_number_notification = ""
        context.bot.send_message(
            chat_id=chat_id,
            text=f"I already remember you! Your id is {chat_id}{phone_number_notification}\n\n{POSSIBLE_OPTIONS}",
        )


def not_initialized(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    context.bot.send_message(chat_id=chat_id, text=UNKNOWN_PERSON_ANS)
