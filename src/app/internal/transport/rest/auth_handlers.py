from django.http import HttpResponse, JsonResponse
from django.views import View
from datetime import datetime
from django.conf import settings

import jwt

from app.internal.services.person_services import check_password
from app.internal.services.issued_token_services import create_tokens, refresh_tokens, revoke_all_user_tokens
from app.models import Person, IssuedToken


class AuthView(View):
    def post(self, request):
        if "Login" not in request.headers or "Password" not in request.headers:
            return HttpResponse("Unauthorized", status=401)
        login = request.headers["Login"]
        password = request.headers["Password"]
        user = Person.objects.filter(username=login).first()
        if not user:
            return HttpResponse("Unauthorized", status=401)
        auth_succeeded = check_password(user.id, password)
        if not auth_succeeded:
            return HttpResponse("Unauthorized", status=401)
        access_token, refresh_token = create_tokens(user.id)
        return JsonResponse({"access_token": access_token, "refresh_token": refresh_token})


class RefreshView(View):
    def post(self, request):
        if "Authorization" in request.headers:
            error, token_object = self.check_authorization(request)
            if error:
                return HttpResponse(error, status=401)
        else:
            return HttpResponse("Unauthorized", status=401)
        access_token, refresh_token = refresh_tokens(token_object)
        return JsonResponse({"access_token": access_token, "refresh_token": refresh_token})
    
    def check_authorization(self, request):
        auth_header = request.headers["Authorization"]
        if len(auth_header) < len("Bearer "):
            return "Unauthorized", None

        token = auth_header[len("Bearer "):]
        payload = self.get_payload(token)
        if not payload:
            return "Unauthorized", None

        user_id, expires_at = payload["user"], datetime.fromisoformat(payload["expires_at"])
        if datetime.now().astimezone() > expires_at:
            revoke_all_user_tokens(user_id)
            return "Token expired", None

        token_object, user_object = self.get_token_and_user_objects(token, user_id)
        if not token_object or not user_object:
            revoke_all_user_tokens(user_id)
            return "Unauthorized", None
        
        if token_object.revoked:
            revoke_all_user_tokens(user_id)
            return "Token revoked", None
        
        return None, token_object
    
    def get_token_and_user_objects(self, token, user_id):
        user = Person.objects.filter(id=user_id).first()
        token = IssuedToken.objects.filter(jti=token).first()
        return token, user
        
    def get_payload(self, token):
        payload = None
        try:
            payload = jwt.decode(token, settings.SECRET, algorithms=["HS256"])
        except:
            return None
        return payload
