from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
from django.views import View
from datetime import datetime
from django.conf import settings

import jwt

from app.internal.infrastructure.serialize import person_info
from app.internal.services.person_services import check_password
from app.internal.services.issued_token_services import create_tokens, refresh_tokens, revoke_all_user_tokens
from app.models import Person, IssuedToken


class MeView(View):
    def get(self, request):
        id = request.bearer_id
        info = person_info(id, False)
        if not info:
            return HttpResponseNotFound(f"No user with id {id}")
        return HttpResponse(info)
