from app.models import Account, Card, Person


def person_accounts_ids(id):
    accounts = Account.objects.filter(owner__id=id).values("id")
    accounts_str = "\n".join(map(lambda x: x["id"], accounts))
    if accounts_str == "":
        return "You have no active accounts"
    else:
        return f"Your active accounts ids:\n\n{accounts_str}"


def account_info(owner_id, account_id):
    account = Account.objects.filter(id=account_id, owner__id=owner_id).first()
    if not account:
        return "You don't have access to this account"
    else:
        return f"account id: {account_id}\nowner id: {owner_id}\naccount balance: {account.balance}"


def person_cards_ids(id):
    cards = Card.objects.filter(account__owner__id=id).values("id").all()
    cards_str = "\n".join(map(lambda x: x["id"], cards))
    if cards_str == "":
        return "You have no active cards"
    else:
        return f"Your active cards ids:\n\n{cards_str}"


def card_info(owner_id, card_id):
    card = (
        Card.objects.filter(id=card_id, account__owner__id=owner_id)
        .values("id", "account__id", "account__balance")
        .first()
    )
    if not card:
        return "You don't have access to this card"
    else:
        return f"card id: {card['id']}\nassociated account id: {card['account__id']}\nowner id: {owner_id}\ncard balance: {card['account__balance']}"


def person_info(id, tg=True):
    sep = "\n" if tg else "<br>"
    person = Person.objects.filter(id=id).first()
    if not person:
        return None
    phone_number = person.phone_number if person.phone_number != "none" else "no phone number yet"
    return f"id: {person.id}{sep}username: {person.username}{sep}phone number: {phone_number}"


def favorites_info(id):
    favs = Person.objects.filter(id=id).prefetch_related("favorites").first()
    favs_list = list(map(lambda x: f"id : {x.id}, username: {x.username}", favs.favorites.all()))
    return favs_list


def serialize_acc_operations(source_acc_id, operations):
    def serialize_acc_operation(source_acc_id, op):
        time = op["time"].strftime("%Y-%m-%d %H:%M")
        if op["source_account__id"] == source_acc_id:
            line = f"\tAt {time} you sent {op['sum']} to account with id {op['recepient_account__id']}"
        else:
            line = f"\tAt {time} you received {op['sum']} from account with id {op['source_account__id']}"
        return line

    return "\n".join([serialize_acc_operation(source_acc_id, op) for op in operations])


def serialize_card_operations(source_card_id, operations):
    def serialize_card_operation(source_card_id, op):
        time = op["time"].strftime("%Y-%m-%d %H:%M")
        if op["source_card__id"] == source_card_id:
            line = f"\tAt {time} you sent {op['sum']} to card with id {op['recepient_card__id']}"
        else:
            line = f"\tAt {time} you received {op['sum']} from card with id {op['source_card__id']}"
        return line

    return "\n".join([serialize_card_operation(source_card_id, op) for op in operations])


def serialize_username_list(usernames):
    return "\n".join(usernames)
