from app.models import Person

from django.contrib.auth.hashers import make_password
from django.conf import settings


SALT = settings.SALT


def person_add_favorite_by_id(user_id, new_fav_id):
    new_fav = Person.objects.filter(id=new_fav_id).first()
    return person_add_favorite(user_id, new_fav)


def person_rem_favorite_by_id(user_id, rem_fav_id):
    fav_to_remove = Person.objects.filter(id=rem_fav_id).first()
    return person_rem_favorite(user_id, fav_to_remove)


def person_add_favorite_by_username(user_id, username):
    fav_to_add = Person.objects.filter(username=username).first()
    return person_add_favorite(user_id, fav_to_add)


def person_rem_favorite_by_username(user_id, username):
    fav_to_rem = Person.objects.filter(username=username).first()
    return person_rem_favorite(user_id, fav_to_rem)


def person_add_favorite(user_id, new_fav):
    if not new_fav:
        return "none"
    if new_fav.id == user_id:
        return "self"
    user = Person.objects.get(id=user_id)
    if user.favorites.filter(id=new_fav.id).exists():
        return "exists"
    user.favorites.add(new_fav)
    return "added"


def person_rem_favorite(user_id, fav_to_remove):
    if not fav_to_remove:
        return "none"
    user = Person.objects.get(id=user_id)
    if not user.favorites.filter(id=fav_to_remove.id).exists():
        return "not exists"
    user.favorites.remove(fav_to_remove)
    return "removed"


def create_password(user_id, password):
    person = Person.objects.filter(id=user_id).first()
    if not person:
        return None
    password = make_password(password, SALT)
    person.password_hash=password
    person.save()
    return "ok"

def check_password(user_id, password):
    person = Person.objects.filter(id=user_id).first()
    if not person:
        return None
    password = make_password(password, SALT)
    expected_password = person.password_hash
    if password != expected_password:
        return None
    return "ok"
