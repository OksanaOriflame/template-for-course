from datetime import datetime

from django.db import transaction

from app.models import Account, AccountOperation, Person


def account_transfer(user_id, account_from_id, account_to_id, transfer_sum):
    if account_from_id == account_to_id:
        return "self"
    account = Account.objects.filter(owner__id=user_id, id=account_from_id).first()
    if not account:
        return "forbidden"
    if transfer_sum > account.balance:
        return "low balance"
    to_account = Account.objects.filter(id=account_to_id).first()
    if not to_account:
        return "no recipient"
    transfer(account, to_account, transfer_sum)
    return "success"


def account_transfer_by_username(user_id, recepient_username, transfer_sum):
    recepient = Person.objects.filter(username=recepient_username).first()
    if not recepient:
        return "no recipient"
    if recepient.id == user_id:
        return "self"
    to_account = Account.objects.filter(owner=recepient).first()
    if not to_account:
        return "no recipient"
    accounts = Account.objects.filter(owner__id=user_id)
    if not accounts.exists():
        return "forbidden"
    account = accounts.filter(balance__gte=transfer_sum).first()
    if not account:
        return "low balance"
    transfer(account, to_account, transfer_sum)
    return "success"


def transfer(account_from, account_to, sum):
    with transaction.atomic():
        account_from.balance = account_from.balance - sum
        account_from.save()
        account_to.balance = account_to.balance + sum
        account_to.save()
        AccountOperation.objects.create(
            source_account=account_from, recepient_account=account_to, time=datetime.now().astimezone(), sum=sum
        )
