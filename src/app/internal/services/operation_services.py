from django.db import connection
from django.db.models import Q

from app.models import Account, AccountOperation, Card, CardOperation, Person

from .status_constants import OpServiceStatus


def card_history(owner_id, card_id):
    if not Card.objects.filter(id=card_id, account__owner__id=owner_id).exists():
        return [], OpServiceStatus.FORBIDDEN
    card_operations_list = (
        CardOperation.objects.filter(Q(source_card=card_id) | Q(recepient_card=card_id))
        .values("source_card__id", "recepient_card__id", "time", "sum")
        .order_by("-time")
        .all()
    )
    if not card_operations_list:
        return [], OpServiceStatus.EMPTY
    print(connection.queries)
    return card_operations_list, OpServiceStatus.OK


def account_history(owner_id, acc_id):
    if not Account.objects.filter(id=acc_id, owner__id=owner_id).exists():
        return [], OpServiceStatus.FORBIDDEN
    acc_operations_list = (
        AccountOperation.objects.filter(Q(source_account=acc_id) | Q(recepient_account=acc_id))
        .values("source_account__id", "recepient_account__id", "time", "sum")
        .order_by("-time")
        .all()
    )
    if not acc_operations_list:
        return [], OpServiceStatus.EMPTY
    return acc_operations_list, OpServiceStatus.OK


def connected_persons(user_id, username):
    acc_pairs = (
        AccountOperation.objects.filter(Q(source_account__owner__id=user_id) | Q(recepient_account__owner__id=user_id))
        .values("source_account__owner__username", "recepient_account__owner__username")
        .order_by("source_account__owner__username", "recepient_account__owner__username")
        .distinct("source_account__owner__username", "recepient_account__owner__username")
        .all()
    )
    owner_usernames = [id for id_dict in acc_pairs for id in id_dict.values()]
    card_pairs = (
        CardOperation.objects.filter(
            Q(source_card__account__owner__id=user_id) | Q(recepient_card__account__owner__id=user_id)
        )
        .values("source_card__account__owner__username", "recepient_card__account__owner__username")
        .order_by("source_card__account__owner__username", "recepient_card__account__owner__username")
        .distinct("source_card__account__owner__username", "recepient_card__account__owner__username")
        .all()
    )
    owner_usernames.extend([id for id_dict in card_pairs for id in id_dict.values()])
    unique_connected_persons_username_list = list(set(filter(lambda x: x != username, owner_usernames)))
    return unique_connected_persons_username_list
