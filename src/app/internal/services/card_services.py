from datetime import datetime

from django.db import transaction

from app.models import Account, Card, CardOperation


def card_transfer(user_id, card_from_id, card_to_id, transfer_sum):
    if card_from_id == card_to_id:
        return "self"
    card = Card.objects.filter(account__owner__id=user_id, id=card_from_id).select_related("account").first()
    if not card:
        return "forbidden"
    to_card = Card.objects.filter(id=card_to_id).select_related("account").first()
    if not to_card:
        return "no recipient"
    account_from_id = card.account.id
    account_to_id = to_card.account.id
    if account_from_id == account_to_id:
        return "self"
    account = Account.objects.filter(owner__id=user_id, id=account_from_id).first()
    if not account:
        return "forbidden"
    if transfer_sum > account.balance:
        return "low balance"
    to_account = Account.objects.filter(id=account_to_id).first()
    if not to_account:
        return "no recipient"
    transfer(account, to_account, card, to_card, transfer_sum)
    return "success"


def transfer(account_from, account_to, card_from, card_to, sum):
    with transaction.atomic():
        account_from.balance = account_from.balance - sum
        account_from.save()
        account_to.balance = account_to.balance + sum
        account_to.save()
        CardOperation.objects.create(
            source_card=card_from, recepient_card=card_to, time=datetime.now().astimezone(), sum=sum
        )
