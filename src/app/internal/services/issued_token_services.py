import datetime

import jwt
from django.conf import settings

from app.models import IssuedToken, Person


ACCESS_TOKEN_LIFETIME = int(settings.ACCESS_TOKEN_EXPIRATION_TIME_MINS)
REFRESH_TOKEN_LIFETIME = int(settings.REFRESH_TOKEN_EXPIRATION_TIME_DAYS)


def revoke_all_user_tokens(user_id):
    IssuedToken.objects.filter(user__id=user_id, revoked=False).update(revoked=True)


def create_tokens(user_id):
    person = Person.objects.filter(id=user_id).first()
    if not person:
        return None
    revoke_all_user_tokens(user_id)
    secret = settings.SECRET

    payload = {"user": user_id, "expires_at": str((datetime.datetime.now() + datetime.timedelta(minutes=ACCESS_TOKEN_LIFETIME)).astimezone())}
    access_token = jwt.encode(payload, secret, algorithm="HS256")

    refresh_token_expires_at = (datetime.datetime.now() + datetime.timedelta(days=REFRESH_TOKEN_LIFETIME)).astimezone()
    payload = {"user": user_id, 
        "expires_at": str(refresh_token_expires_at)}
    refresh_token = jwt.encode(payload, secret, algorithm="HS256")

    IssuedToken.objects.create(
        jti=refresh_token,
        user=person,
        expires_at=refresh_token_expires_at
    )

    return access_token, refresh_token


def refresh_tokens(refresh_token_object):
    bearer_id = refresh_token_object.user.id
    revoke_all_user_tokens(bearer_id)
    return create_tokens(bearer_id)
