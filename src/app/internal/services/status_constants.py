from enum import Enum


class OpServiceStatus(Enum):
    FORBIDDEN = 1
    EMPTY = 2
    OK = 3
