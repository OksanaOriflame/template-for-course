from telegram.ext import CommandHandler, MessageHandler, Updater
from telegram.ext.filters import Filters

from app.internal.bot_filters.not_initialized_filter import Not_initialized_filter
from app.internal.bot_filters.setting_phone_status_filter import Setting_phone_filter
from app.internal.transport.bot.accounts_handler import account_info_handler, accounts
from app.internal.transport.bot.auth_handler import login
from app.internal.transport.bot.cards_handlers import card_info_handler, cards
from app.internal.transport.bot.favorites_handlers import (
    add_favorite_by_id,
    add_favorite_by_username,
    favorites,
    rem_favorite_by_id,
    rem_favorite_by_username,
)
from app.internal.transport.bot.history_handlers import (
    account_history_handler,
    card_history_handler,
    connected_persons_handler,
)
from app.internal.transport.bot.info_handlers import me, unrecognized
from app.internal.transport.bot.set_phone_handlers import set_phone, set_phone_start
from app.internal.transport.bot.start_handlers import not_initialized, start
from app.internal.transport.bot.transfer_handlers import transfer_account, transfer_card, transfer_info, transfer_user


def init_tg_bot_updater(token):
    updater = Updater(token=token)
    dispatcher = updater.dispatcher

    start_handler = CommandHandler("start", start)
    not_initialized_handler = MessageHandler(Not_initialized_filter(), not_initialized)
    set_phone_start_handler = CommandHandler("set_phone", set_phone_start)
    set_phone_handler = MessageHandler(Setting_phone_filter(), set_phone)
    me_handler = CommandHandler("me", me)

    cards_handler = CommandHandler("cards", cards)
    card_info_handler_ = CommandHandler("card", card_info_handler)
    accounts_handler = CommandHandler("accounts", accounts)
    account_info_handler_ = CommandHandler("account", account_info_handler)

    favorites_list_handler = CommandHandler("favorites", favorites)
    add_favorite_by_id_handler = CommandHandler("addfav", add_favorite_by_id)
    remove_favorite_by_id_handler = CommandHandler("remfav", rem_favorite_by_id)
    add_favorite_by_username_handler = CommandHandler("addfavname", add_favorite_by_username)
    remove_favorite_by_username_handler = CommandHandler("remfavname", rem_favorite_by_username)

    transfer_info_handler = CommandHandler("transferinfo", transfer_info)
    transfer_card_to_card_handler = CommandHandler("cardtransfer", transfer_card)
    transfer_acc_to_acc_handler = CommandHandler("acctransfer", transfer_account)
    transfer_user_to_user_handler = CommandHandler("usertransfer", transfer_user)

    card_history_handler_ = CommandHandler("cardhistory", card_history_handler)
    account_history_handler_ = CommandHandler("acchistory", account_history_handler)
    connected_persons_handler_ = CommandHandler("connections", connected_persons_handler)

    login_handler = CommandHandler("login", login)

    default_handler = MessageHandler(Filters.all, unrecognized)

    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(not_initialized_handler)
    dispatcher.add_handler(set_phone_start_handler)
    dispatcher.add_handler(set_phone_handler)
    dispatcher.add_handler(me_handler)

    dispatcher.add_handler(cards_handler)
    dispatcher.add_handler(card_info_handler_)
    dispatcher.add_handler(accounts_handler)
    dispatcher.add_handler(account_info_handler_)

    dispatcher.add_handler(favorites_list_handler)
    dispatcher.add_handler(add_favorite_by_id_handler)
    dispatcher.add_handler(remove_favorite_by_id_handler)
    dispatcher.add_handler(add_favorite_by_username_handler)
    dispatcher.add_handler(remove_favorite_by_username_handler)

    dispatcher.add_handler(transfer_info_handler)
    dispatcher.add_handler(transfer_card_to_card_handler)
    dispatcher.add_handler(transfer_acc_to_acc_handler)
    dispatcher.add_handler(transfer_user_to_user_handler)

    dispatcher.add_handler(card_history_handler_)
    dispatcher.add_handler(account_history_handler_)
    dispatcher.add_handler(connected_persons_handler_)

    dispatcher.add_handler(login_handler)

    dispatcher.add_handler(default_handler)

    return updater
