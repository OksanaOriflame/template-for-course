from .bot import init_tg_bot_updater


def start_bot_polling(token):
    updater = init_tg_bot_updater(token)
    updater.start_polling()
