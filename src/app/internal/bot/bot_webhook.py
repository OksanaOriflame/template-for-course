from .bot import init_tg_bot_updater


def get_bot(token):
    updater = init_tg_bot_updater(token)
    return updater


def start_bot_webhook(token, host, port, base_url):
    updater = get_bot(token)
    updater.start_webhook(listen=host, port=port, url_path=token, webhook_url=f"{base_url}/{token}")
    updater.idle()
