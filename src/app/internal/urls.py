from django.urls import path

from app.internal.transport.rest.handlers import MeView
from app.internal.transport.rest.auth_handlers import AuthView, RefreshView

urlpatterns = [path("me", MeView.as_view()),
               path("auth", AuthView.as_view()),
               path("refresh", RefreshView.as_view()),]
