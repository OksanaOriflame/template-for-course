from datetime import datetime

import jwt
from django.conf import settings
from django.contrib.auth.backends import BaseBackend
from django.http import HttpResponse


NO_AUTH_ALLOWED_PATHS = {"/api/refresh", "/api/auth"}


class AuthMiddleware(BaseBackend):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        bearer_id = None
        if request.path in NO_AUTH_ALLOWED_PATHS:
            return self.get_response(request)
        if "Authorization" in request.headers:
            auth_header = request.headers["Authorization"]
            if len(auth_header) < len("Bearer "):
                return HttpResponse("Unauthorized", status=401)
            token = auth_header[len("Bearer "):]
            bearer_id, check_message = self.check_access_token(token)
            if not bearer_id:
                return HttpResponse(check_message, status=401)
        else:
            return HttpResponse("Unauthorized", status=401)
        request.bearer_id = bearer_id
        return self.get_response(request)

    def check_access_token(self, token):
        payload = None
        try:
            payload = jwt.decode(token, settings.SECRET, algorithms=["HS256"])
        except:
            return None, "Bad access token"
        user_id = payload["user"]
        if datetime.fromisoformat(payload["expires_at"]) < datetime.now().astimezone():
            return None, "Token has been expired"
        return user_id, "ok"
