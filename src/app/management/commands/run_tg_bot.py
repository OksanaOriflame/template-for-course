import os

import environ
from django.core.management.base import BaseCommand

from app.internal.bot.bot_webhook import start_bot_webhook


class Command(BaseCommand):
    def handle(self, *args, **options):
        env = environ.Env()
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
        environ.Env.read_env(os.path.join(BASE_DIR, ".env"))

        start_bot_webhook(env("TG_BOT_TOKEN"), env("TG_BOT_HOST"), env("TG_BOT_PORT"), env("BASE_URL"))
