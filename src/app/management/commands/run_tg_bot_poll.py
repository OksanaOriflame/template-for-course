import os

import environ
from django.core.management.base import BaseCommand

from app.internal.bot.bot_poll import start_bot_polling


class Command(BaseCommand):
    def handle(self, *args, **options):
        env = environ.Env()
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
        environ.Env.read_env(os.path.join(BASE_DIR, ".env"))

        start_bot_polling(env("TG_BOT_TOKEN"))
