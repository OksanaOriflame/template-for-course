from typing import Optional

from django.contrib import admin
from django.http import HttpRequest

from app.internal.admin.admin_user import AdminUserAdmin

from .models import Account, AccountOperation, Card, CardOperation, IssuedToken, Person

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    pass


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    pass


@admin.register(AccountOperation)
class AccountOperationAdmin(admin.ModelAdmin):
    def has_add_permission(self, request: HttpRequest) -> bool:
        return False

    def has_change_permission(self, request: HttpRequest, obj=None) -> bool:
        return False

    def has_delete_permission(self, request: HttpRequest, obj=None) -> bool:
        return False


@admin.register(CardOperation)
class CardOperationAdmin(admin.ModelAdmin):
    def has_add_permission(self, request: HttpRequest) -> bool:
        return False

    def has_change_permission(self, request: HttpRequest, obj=None) -> bool:
        return False

    def has_delete_permission(self, request: HttpRequest, obj=None) -> bool:
        return False


@admin.register(IssuedToken)
class IssuedTokenAdmin(admin.ModelAdmin):
    pass
