build:
	docker-compose build

up:
	docker-compose up -d

makemigrations:
	docker-compose run -T app pipenv run python manage.py makemigrations

migrate:
	docker-compose run -T app pipenv run python manage.py migrate

collectstatic:
	docker-compose run -T app pipenv run python manage.py collectstatic --noinput --clear

run_server:
	docker-compose exec app pipenv run python manage.py runserver 0.0.0.0:8080

run_tg_bot:
	docker-compose exec bot pipenv run python manage.py run_tg_bot

createsuperuser:
	echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('${SUPERUSER_LOGIN}', '${SUPERUSER_MAIL}', '${SUPERUSER_PASSWORD}')" | docker-compose exec -T app pipenv run python manage.py shell

flush:
	docker-compose run -T app pipenv run python manage.py flush --noinput

down:
	docker-compose down

shell:
	docker-compose exec app pipenv run python manage.py shell

lint:
	docker-compose run app pipenv run isort .
	docker-compose run app pipenv run flake8 --config setup.cfg
	docker-compose run app pipenv run black --config pyproject.toml .

no-lint:
	echo 'still no lint'

check_lint:
	docker-compose run app pipenv run isort --check --diff .
	docker-compose run app pipenv run flake8 --config setup.cfg
	docker-compose run app pipenv run black --check --config pyproject.toml .

closeapp:
	docker-compose down

pull:
	docker pull ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}

push:
	docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}

test:
	docker-compose run app pipenv run bash -c "cd ./test && pytest"